#!/bin/sh
set -ex
export KUBECONFIG=./kubeconfig

kubectl config use-context do-sfo2-prod1
kubectl apply -f k8s/namespaces.yaml
kubectl apply -Rf k8s/operators
# wait for cert-manager to finish rolling out. If other operators also cause problems, add them here
kubectl -n cert-manager rollout status deployment/cert-manager-webhook -w
kubectl apply -Rf k8s/
