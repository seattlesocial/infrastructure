#!/usr/bin/env python3
# shitty terraform, because terraform doesn't seem to like being stateless and
# I dont want to haul around a silly state file for something that really shouldn't
# need it
import requests
import time
import logging
import json
import base64
import configparser
import sys

import boto3


logging.basicConfig(level=logging.INFO)
config = configparser.ConfigParser()
if len(sys.argv) > 1:
    config.read_file(open(sys.argv[1]))
else:
    config.read_file(open('settings.ini'))

USER_AGENT = "{} - contact abuse@social.seattle.wa.us".format(__file__)
SECRETS = ["AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY", "SECRET_KEY_BASE", "OTP_SECRET", "VAPID_PRIVATE_KEY", "VAPID_PUBLIC_KEY", "SMTP_PASSWORD"]

HEADERS = {
    "Authorization": "Bearer {}".format(config['digitalocean']['token']),
    "User-Agent": __file__
}

POSTHEADERS = HEADERS
POSTHEADERS['Content-Type'] = "application/json"


k8s_id = None
db_id = None
lb_id = None
cert_id = None
domain_id = None

session = boto3.session.Session()
client = session.client('s3', region_name=config['digitalocean']['region'],
                        endpoint_url="https://{}.digitaloceanspaces.com".format(config['digitalocean']['region']),
                        aws_access_key_id=config['mastodon']['aws_access_key_id'],
                        aws_secret_access_key=config['mastodon']['aws_secret_access_key'])


def get_k8s_id():
    global k8s_id
    res = requests.get("https://api.digitalocean.com/v2/kubernetes/clusters", headers=HEADERS).json()
    for cluster in res.get('kubernetes_clusters', []):
        if cluster['name'] == config['digitalocean']['name']:
            k8s_id = cluster['id']


def create_k8s():
    global k8s_id
    body = {
        "name": config['digitalocean']['name'],
        "region": config['digitalocean']['region'],
        "version": config['digitalocean']['k8s_version'],
        "auto_upgrade": True,
        "tags": ["dev"],
        "node_pools": [{
            "size": config['digitalocean']['k8s_node_size'],
            "name": "{}-nodes".format(config['digitalocean']['name']),
            "count": int(config['digitalocean']['k8s_node_count'])
        }]
    }
    headers = HEADERS
    headers["Content-Type"] = "application/json"
    res = requests.post("https://api.digitalocean.com/v2/kubernetes/clusters", headers=headers, data=json.dumps(body))
    if not res.ok:
        logging.info("Error creating cluster: %s", res.text)
    k8s_id = res.json()['kubernetes_cluster']['id']


def wait_for_k8s_and_db():
    k8s_state = ""
    db_state = ""
    while k8s_state != "running" or db_state != "online":
        t = 0
        if k8s_state != "running":
            res = requests.get("https://api.digitalocean.com/v2/kubernetes/clusters/{}".format(k8s_id), headers=HEADERS).json()
            k8s_state = res['kubernetes_cluster']['status']['state']
            logging.info("Waiting for k8s cluster %s... (current state: %s)", k8s_id, k8s_state)
            t = 10

        if db_state != "online":
            res = requests.get("https://api.digitalocean.com/v2/databases/{}".format(db_id), headers=HEADERS).json()
            db_state = res['database']['status']
            logging.info("Waiting for db %s... (current state: %s)", db_id, db_state)
            t = 10

        if k8s_state != "running" or db_state != "online":
            time.sleep(t)


def get_db_id():
    global db_id
    res = requests.get("https://api.digitalocean.com/v2/databases", headers=HEADERS).json()
    for db in res.get('databases', []):
        if db['name'] == config['digitalocean']['name']:
            db_id = db['id']


def create_db():
    global db_id
    body = {
        "name": config['digitalocean']['name'],
        "engine": "pg",
        "version": "10",
        "region": config['digitalocean']['region'],
        "size": config['digitalocean']['db_node_size'],
        "num_nodes": int(config['digitalocean']['db_node_count'])
    }
    headers = HEADERS
    headers["Content-Type"] = "application/json"
    r = requests.post("https://api.digitalocean.com/v2/databases",
                      headers=headers, data=json.dumps(body))
    if not r.ok:
        r.raise_for_status()
    res = r.json()
    if "database" not in res:
        logging.error("Failed to create database cluster: %s", json.dumps(res))
        raise Exception("Failed to create database cluster")
    db_id = res['database']['id']


def ensure_db_db():
    url = "https://api.digitalocean.com/v2/databases/{}/dbs".format(db_id)
    if requests.get("{}/mastodon".format(url), headers=HEADERS).ok:
        return
    logging.info("Creating database mastodon on database cluster %s", db_id)
    r = requests.post(url, headers=POSTHEADERS, data=json.dumps({"name": "mastodon"}))
    if not r.ok:
        logging.error("Failed to create database: %s", r.text)
        r.raise_for_status()


def ensure_db_user():
    url = "https://api.digitalocean.com/v2/databases/{}/users".format(db_id)
    if requests.get("{}/mastodon".format(url), headers=HEADERS).ok:
        return
        logging.info("Creating user mastodon on database cluster %s", db_id)
    requests.post(url, headers=POSTHEADERS, data=json.dumps({"name": "mastodon"})).raise_for_status()


def ensure_db_pool():
    url = "https://api.digitalocean.com/v2/databases/{}/pools".format(db_id)
    if requests.get("{}/mastodon".format(url), headers=HEADERS).ok:
        return

    body = {
      "name": "mastodon",
      "mode": "transaction",
      "size": 10,
      "db": "mastodon",
      "user": "mastodon"
    }
    logging.info("Createing database pool mastodon on database %s", db_id)
    requests.post(url, headers=POSTHEADERS, data=json.dumps(body)).raise_for_status()


def existing_buckets():
    return [x['Name'] for x in client.list_buckets()['Buckets']]


def get_certificate_id():
    res = requests.get("https://api.digitalocean.com/v2/certificates", headers=HEADERS).json()
    for cert in res.get('certificates', []):
        if config['mastodon']['s3_alias_host'] in cert['dns_names']:
            return cert['id']


def create_certificate():
    body = {
        "name": config['digitalocean']['name'],
        "type": "lets_encrypt",
        "dns_names": [config['mastodon']['s3_alias_host']]
    }
    headers = HEADERS
    headers["Content-Type"] = "application/json"
    logging.info("Creating certificate for %s", ", ".join(body['dns_names']))
    r = requests.post("https://api.digitalocean.com/v2/certificates", headers=headers, data=json.dumps(body))
    if not r.ok:
        logging.error("Failed to create certificate: %s", r.text)
        r.raise_for_status()
    res = r.json()
    if "certificate" not in res:
        logging.error("Failed to create certificate: %s", json.dumps(res))
        raise Exception("Failed to create certificate")
    logging.info("Created certificate")


def ensure_media_domain():
    s3_alias_host = config['mastodon']['s3_alias_host']
    if not s3_alias_host.endswith(config['digitalocean']['dns_zone']):
        raise Exception("s3_alias_host (%s) be in the DNS zone %s", s3_alias_host, config['digitalocean']['dns_zone'])
    s3_alias_name = s3_alias_host.replace("." + config['digitalocean']['dns_zone'], "")
    records_url = "https://api.digitalocean.com/v2/domains/{}/records".format(config['digitalocean']['dns_zone'])
    res = requests.get(records_url, headers=HEADERS).json()
    for record in res.get('domain_records', []):
        if record['name'] == s3_alias_name:
            return

    body = {
        "type": "CNAME",
        "name": s3_alias_name,
        "data": "{}.{}.cdn.digitaloceanspaces.com.".format(config['digitalocean']['name'], config['digitalocean']['region']),
        "ttl": 60
    }

    headers = HEADERS
    headers["Content-Type"] = "application/json"
    logging.info("Creating s3_alias_host DNS record %s", s3_alias_host)
    res = requests.post(records_url, headers=headers, data=json.dumps(body)).json()
    if "domain_record" not in res:
        logging.error("Failed to create s3_alias_host DNS record: %s", json.dumps(res))
        raise Exception("Failed to create s3_alias_host DNS records")
    logging.info("Created s3_alias_host DNS record")


get_db_id()
if db_id is None:
    create_db()

get_k8s_id()
if k8s_id is None:
    create_k8s()

if config['digitalocean']['name'] not in existing_buckets():
    logging.info("Creating bucket %s", config['digitalocean']['name'])
    client.create_bucket(Bucket=config['digitalocean']['name'], ACL='public-read')

ensure_media_domain()
wait_for_k8s_and_db()

ensure_db_db()
ensure_db_user()
ensure_db_pool()


with open("k8s/secrets/mastodon-credentials.json", "w") as f:
    json.dump({
       "apiVersion": "v1",
       "kind": "Secret",
       "metadata": {
         "name": "mastodon-credentials",
         "namespace": "mastodon"
       },
       "type": "Opaque",
       "data":  dict([(k, base64.b64encode(config['mastodon'][k.lower()].encode('utf-8')).decode()) for k in SECRETS])
    }, f, indent=4)

pool = requests.get("https://api.digitalocean.com/v2/databases/{}/pools".format(db_id), headers=HEADERS).json()['pools'][0]
# DigitalOcean API returns the wrong credentials for the private connection, so we use the public connection string and replace the hosts
db_uri = pool['connection']['uri'].replace(pool['connection']['host'], pool['private_connection']['host'])
db_uri.replace("sslmode=require", "sslmode=verify-full")

for ns in ["mastodon", "prometheus"]:
    with open("k8s/secrets/database-{}.json".format(ns), "w") as f:
        json.dump({
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {
                "name": "database",
                "namespace": ns
            },
            "type": "Opaque",
            "data": {
                "DATABASE_URL": base64.b64encode(db_uri.encode('utf-8')).decode(),
            }
        }, f, indent=4)

# Migration job doesn't work through pgbouncer
with open("k8s/secrets/migrate-database.json", "w") as f:
    pgbouncer_port = pool['connection']['port']
    migrate_db_uri = db_uri.replace(str(pgbouncer_port), str(pgbouncer_port-1))
    json.dump({
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": "migrate-database",
            "namespace": "mastodon"
        },
        "type": "Opaque",
        "data": {
            "DATABASE_URL": base64.b64encode(migrate_db_uri.encode('utf-8')).decode(),
            "DB_SSLMODE": base64.b64encode(b"require").decode()
        }
    }, f, indent=4)

with open("k8s/secrets/dockerconfig.json", "w") as f:
    auth = "{}:{}".format(config['registry']['username'], config['registry']['password']).encode('utf-8')
    dockerconfigjson = json.dumps({
      "auths": {
        "registry.gitlab.com": {
          "username": config['registry']['username'],
          "password": config['registry']['password'],
          "auth":  base64.b64encode(auth).decode()
        }
      }
    }).encode('utf-8')
    json.dump({
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": "gitlab",
            "namespace": "mastodon"
        },
        "type": "kubernetes.io/dockerconfigjson",
        "data": {
            ".dockerconfigjson": base64.b64encode(dockerconfigjson).decode()
        }
    }, f, indent=4)

for ns in ["external-dns", "mastodon", "cert-manager", "gitlab-runner", "prometheus"]:
    with open("k8s/secrets/digitalocean-token-{}.json".format(ns), "w") as f:
        json.dump({
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {
                "name": "digitalocean",
                "namespace": ns
            },
            "type": "Opaque",
            "data": {
                "DIGITALOCEAN_TOKEN": base64.b64encode(config['digitalocean']['token'].encode('utf-8')).decode()
            }
        }, f, indent=4)

# 
# with open("k8s/secrets/grafana-oauth.json", "w") as f:
#     json.dump({
#         "apiVersion": "v1",
#         "kind": "Secret",
#         "metadata": {
#             "name": "grafana-oauth",
#             "namespace": "grafana"
#         },
#         "type": "Opaque",
#         "data": {
#             "CLIENT_ID": base64.b64encode(config['grafana']['client_id'].encode('utf-8')).decode(),
#             "CLIENT_SECRET": base64.b64encode(config['grafana']['client_secret'].encode('utf-8')).decode()
#         }
#     }, f, indent=4)

with open("k8s/secrets/gitlab-runner.json", "w") as f:
    json.dump({
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": "gitlab-runner",
            "namespace": "gitlab-runner"
        },
        "type": "Opaque",
        "data": {
            "REGISTRATION_TOKEN": base64.b64encode(config['gitlab']['runner_token'].encode('utf-8')).decode(),
        }
    }, f, indent=4)

# Save the kubeconfig
kubeconfig = requests.get("https://api.digitalocean.com/v2/kubernetes/clusters/{}/kubeconfig".format(k8s_id), headers=HEADERS)
with open("kubeconfig", "w") as f:
    f.write(kubeconfig.text)
