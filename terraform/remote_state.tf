terraform {
  backend "s3" {
    bucket      = "seattle-social-tf-state"
    key         = "infrastructure"
    endpoint    = "sfo2.digitaloceanspaces.com"
    region      = "us-east-1"
    skip_credentials_validation = true
  }
}
