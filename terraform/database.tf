resource "digitalocean_database_cluster" "prod1" {
  name       = "prod1"
  engine     = "pg"
  version    = "11"
  size       = "db-s-1vcpu-2gb"
  region     = "sfo2"
  node_count = 1
}
