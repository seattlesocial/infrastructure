apiVersion: apps/v1
kind: Deployment
metadata:
  name: mastodon-streaming
  namespace: mastodon
  labels:
    app: mastodon
    component: streaming
spec:
  strategy:
    rollingUpdate:
      maxUnavailable: 1
  selector:
    matchLabels:
      app: mastodon
      component: streaming
  template:
    metadata:
      labels:
        app: mastodon
        component: streaming
    spec:
      imagePullSecrets:
        - name: gitlab
      containers:
      - name: mastodon
        image: docker.io/tootsuite/mastodon-streaming:v4.3.0
        command: ["yarn", "start"]
        ports:
        - containerPort: 4000
        envFrom:
        - configMapRef:
            name: mastodon
        - secretRef:
            name: mastodon-credentials
        - secretRef:
            name: database
        - secretRef:
            name: mastodon-secrets
        env:
          - name: DB_SSLMODE
            value: verify
          - name: NODE_EXTRA_CA_CERTS
            value: /opt/database-tls/db-ca.crt
        livenessProbe:
          httpGet:
            path: /api/v1/streaming/health
            port: 4000
            httpHeaders:
            - name: x-forwarded-proto
              value: https
          initialDelaySeconds: 30
          periodSeconds: 30
        resources:
          requests:
            cpu: 150m
            memory: 200Mi
          limits:
            cpu: 750m
            memory: 800Mi
        volumeMounts:
          - name: database-ca
            mountPath: /opt/database-tls
      volumes:
        - name: database-ca
          configMap:
            name: database-ca
---
apiVersion: v1
kind: Service
metadata:
  name: mastodon-streaming
  namespace: mastodon
  annotations:
    prometheus.io/port: "4000"
spec:
  selector:
    app: mastodon
    component: streaming
  ports:
  - protocol: TCP
    port: 4000
    targetPort: 4000
---
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: mastodon-streaming
  namespace: mastodon
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: mastodon-streaming
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 90
  behavior:
    scaleUp:
      stabilizationWindowSeconds: 120
      policies:
      - type: Pods # add 1 pod every 30 seconds
        value: 1
        periodSeconds: 30
      - type: Percent # increase number of pods by 10% every 30 seconds
        value: 10
        periodSeconds: 30
      selectPolicy: Max # select the policy that allows adding the maximum number of pods
    scaleDown:
      stabilizationWindowSeconds: 300
      policies:
      - type: Percent # decrease the number of pods by 10% every 30 seconds
        value: 10
        periodSeconds: 60
      - type: Pods # remove 1 pod every 60 seconds
        value: 1
        periodSeconds: 60
      selectPolicy: Max # select the policy that allows removing the maximum number of pods

