apiVersion: apps/v1
kind: Deployment
metadata:
  name: statsd-exporter
  namespace: prometheus
spec:
  replicas: 1
  selector:
    matchLabels:
      app: statsd-exporter
  template:
    metadata:
      labels:
        app: statsd-exporter
    spec:
      containers:
      - name: statsd-exporter
        image: prom/statsd-exporter
        args:
          - --statsd.mapping-config=/config/mappings.yaml
        ports:
        - containerPort: 9102
        - containerPort: 9125
        resources:
          requests:
            cpu: 30m
            memory: 150Mi
          limits:
            cpu: 100m
            memory: 250Mi
        volumeMounts:
        - name: config
          mountPath: /config
      volumes:
      - name: config
        configMap:
          name: statsd-exporter
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: statsd-exporter
  namespace: prometheus
data:
  mappings.yaml: |-
    mappings:
      ## Web collector
    - match: Mastodon\.(.+)\.web\.(.+)\.(.+)\.(.+)\.status\.(.+)
      match_type: regex
      name: "mastodon_controller_status"
      labels:
        env: $1
        controller: $2
        action: $3
        format: $4
        status: $5
        mastodon: web
    - match: Mastodon\.(.+)\.web\.(.+)\.(.+)\.(.+)\.db_time
      match_type: regex
      name: "mastodon_controller_db_time"
      labels:
        env: $1
        controller: $2
        action: $3
        format: $4
        mastodon: web
    - match: Mastodon\.(.+)\.web\.(.+)\.(.+)\.(.+)\.view_time
      match_type: regex
      name: "mastodon_controller_view_time"
      labels:
        env: "$1"
        controller: $2
        action: $3
        format: $4
        mastodon: web
    - match: Mastodon\.(.+)\.web\.(.+)\.(.+)\.(.+)\.total_duration
      match_type: regex
      name: "mastodon_controller_duration"
      labels:
        env: "$1"
        controller: $2
        action: $3
        format: $4
        mastodon: web
    - match: "Mastodon.*.db.tables.*.queries.*.duration"
      name: "mastodon_db_query"
      labels:
        env: "$1"
        table: "$2"
        query: "$3"
    - match: "Mastodon.*.sidekiq.ActivityPub.*.*.processing_time"
      name: "mastodon_sidekiq_processing_time"
      labels:
        env: "$1"
        sidekiq_job: "$2"
    - match: "Mastodon.*.sidekiq.ActivityPub.*.*.success"
      name: "mastodon_sidekiq_success"
      labels:
        env: "$1"
        nothing: "$2"
        sidekiq_job: "$3"
    - match: "Mastodon.*.sidekiq.queues.*.enqueued"
      name: "mastodon_sidekiq_enqueued_jobs"
      labels:
        env: "$1"
        queue: "$2"
    - match: "Mastodon.*.sidekiq.queues.*.latency"
      name: "mastodon_sidekiq_latency"
      labels:
        env: "$1"
        queue: "$2"
    - match: "Mastodon.(.*).sidekiq.(processed|failed|busy|enqueued|scheduled|retries|dead)(_size|)"
      match_type: regex
      name: mastodon_sidekiq_jobs_total
      labels:
        env: "$1"
        state: "$2"
    - match: "Mastodon.*.sidekiq.processes"
      name: "mastodon_sidekiq_processes"
      labels:
        env: "$1"
---
apiVersion: v1
kind: Service
metadata:
  name: statsd-exporter
  namespace: prometheus
  labels:
    app: statsd-exporter
  annotations:
    prometheus.io/port: "9102"
spec:
  ports:
  - name: udp
    port: 9125 # port to push metrics to
    protocol: UDP
    targetPort: 9125
  - name: tcp
    port: 9102 # port to fetch metrics from
    protocol: TCP
    targetPort: 9102
  selector:
    app: statsd-exporter
