#!/usr/bin/env python3
# shitty terraform, because terraform doesn't seem to like being stateless and
# I dont want to haul around a silly state file for something that really shouldn't
# need it
import requests
import logging
import configparser


logging.basicConfig(level=logging.INFO)
config = configparser.ConfigParser()
config.read_file(open('settings.ini'))

USER_AGENT = "{} - contact abuse@social.seattle.wa.us".format(__file__)

HEADERS = {
    "Authorization": "Bearer {}".format(config['digitalocean']['token']),
    "User-Agent": __file__
}


def delete_k8s():
    res = requests.get("https://api.digitalocean.com/v2/kubernetes/clusters", headers=HEADERS).json()
    for cluster in res.get('kubernetes_clusters', []):
        if cluster['name'] == config['digitalocean']['name']:
            logging.info("Deleting k8s cluster %s", cluster['id'])
            r = requests.delete("https://api.digitalocean.com/v2/kubernetes/clusters/{}".format(cluster['id']), headers=HEADERS)
            if not r.ok:
                logging.warn("Failed to delete k8s cluter %s: %s", cluster['id'], r.text)


def delete_db():
    global db_id
    res = requests.get("https://api.digitalocean.com/v2/databases", headers=HEADERS).json()
    for db in res.get('databases', []):
        if db['name'] == config['digitalocean']['name']:
            logging.info("Deleting database %s", db['id'])
            r = requests.delete("https://api.digitalocean.com/v2/databases/{}".format(db['id']), headers=HEADERS)
            if not r.ok:
                logging.warn("Failed to delete database %s: %s", db['id'], r.text)


delete_k8s()
delete_db()
