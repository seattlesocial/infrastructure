# infrastructure

Usage:

1. Copy `settings.example.ini` to `settings.ini`. Fill out all the values in `settings.ini`. The values are documented
     lower in this document.
2. Create the resources in DigitalOcean: `python3 ensure-cloud-resources.py`. This will also create a number of
     Kubernetes Secrets objects in `k8s/secrets/` and a `kubeconfig` in the current directory. It will run for a few
     minutes while the resources are created.
3. Create the Kubernetes resources in `k8s/` by running `./apply-configs.sh`. This will create the resources in the
     correct order.

**Note: Currently the script applies the configs too fast, and may fail when creating all resources for the first time.
  If you encounter an issue, run the script again**

## `settings.ini`

`settings.ini` contains all the settings used in the python scripts. An example config is included in `settings.example.ini`.
They may also need to be changed in different files in the `k8s/` directory.

### `[digitalocean]`
* `token` - Your DigitalOcean API token. These can be created from [the API page in the DigitalOcean management UI](https://cloud.digitalocean.com/account/api/tokens)
* `name` - The name assigned to most created resources. Also used when cleaning up
* `region` - The DigitalOcean region to deploy all resources to
* `k8s_version` - The DigitalOcean Kubernetes version
* `k8s_node_count` - The number of nodes in the Kubernetes cluster
* `db_version` - The version of postgres to use
* `db_node_size` - The node size to use for the managed database
* `db_node_count` - The number of nodes in the managed database cluster
* `dns_zone` - The DNS zone as that is registered in DigitalOcean. Domain will be `<name>.<dns_zone>`

### `[mastodon]`
* `s3_alias_host` - The media domain
* `aws_access_key_id` - The DigitalOcean Spaces key ID.
* `aws_secret_access_key` - The DigitalOcean Spaces secret key
* `secret_key_base` - The secret key. Generate with `RAILS_ENV=production bundle exec rake secret`
* `otp_secret` - The OTP secret. Generate with `RAILS_ENV=production bundle exec rake secret`. `otp_secret` and
     `secret_key_base` should be different (run the command twice)
* `vapid_public_key` - The VAPID public key. Generate with this and `vapid_private_key` with
     `RAILS_ENV=production bundle exec rake mastodon:webpush:generate_vapid_key`.
* `vapid_private_key` - The VAPID private key. Generated when `vapid_public_key` is generated.
* `smpt_password` - The SMTP password.
